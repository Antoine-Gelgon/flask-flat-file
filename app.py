from flask import Flask, render_template
import glob
import yaml
from slugify import slugify
import markdown

app = Flask(__name__)

path_mds = 'docs/'
file_conf = 'config.yaml'

def get_yaml(f):
  pointer = f.tell()
  if f.readline() != '---\n':
    f.seek(pointer)
    return ''
  readline = iter(f.readline, '')
  readline = iter(readline.__next__, '---\n')
  return ''.join(readline)

def parseFile(path_file):
    with open(path_file, encoding='UTF-8') as f:
        config = list(yaml.load_all(get_yaml(f), Loader=yaml.SafeLoader)) 
        text = f.read()
    return config, text

def getConfig():
    with open(file_conf, encoding='UTF-8') as f:
        config = yaml.safe_load(f.read())
        print(config)
    return config

getConfig()

def getNav():
    nav_list = []
    nav_files = glob.glob(path_mds+"*.yaml") 
    for f in nav_files:
        c, f = parseFile(f)
        c[0]['url'] = slugify(c[0]['title'])
        c[0]['content'] = markdown.markdown(f)
        nav_list.append(c[0])
    return nav_list

def getPage(page):
    for item in getNav():
        if item['url'] == page:
            print('--------> ', item['url'])
            return item
 

@app.route('/')
def index():
    return render_template('index.html', config=getConfig(), nav=getNav())

@app.route('/<page>')
def page(page):
    getPage(page)
    return render_template('page.html', config=getConfig(), nav=getNav(), page=getPage(page))

app.run(debug=True)
